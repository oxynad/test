# LINGI2142 Group 6

## Important

You have to give executation permission to *_boot and *_start files in cfg/

run : ```chmod +x *_boot ```
then: ```chmod +x *_start``

## Useful commands


ip link show —> show all network interfaces

ifconfig —> enable/disable interface on demand

netstat -r

route

ping6 <ipv6 address>

ip -6 route

ip -6 address



## Useful softwares

Quagga

bird6

radvd

ospf6d

## Findings

To ping a neighbor machine (ping HALL from PYTH):

Run this command on HALL node

```
ifconfig
```

Find the ipv6 address of the outgoing link from HALL to PYTH (HALL-eth1 in this case)

Then go on the PYTH node and run this command

```
ping6 -I PYTH-eth0 -c 5 fe80::fc0d:e0ff:fe91:8c25
```


the '-I' argument defines the source interface through which the ping will be sent

the '-c 5' argument defines the number of ping requests to send 

'fe80::fc0d:e0ff:fe91:8c25' is the ipv6 address of the outgoing link from HALL to PYTH

Open a new terminal and go on the HALL node and run this command

```
tcpdump -vi HALL-eth1
``

You should now see HALL receiving ping requests from PYTH

